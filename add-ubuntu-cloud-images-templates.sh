#!/bin/bash

source common/functions.sh

# Ubuntu versions
create_cloudimage_template https://cloud-images.ubuntu.com/trusty/current/trusty-server-cloudimg-amd64-disk1.img 2000 "Ubuntu 14.04 LTS Cloud Image"
create_cloudimage_template https://cloud-images.ubuntu.com/xenial/current/xenial-server-cloudimg-amd64-disk1.img 2001 "Ubuntu 16.04 LTS Cloud Image"

create_cloudimage_template https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img 2002 "Ubuntu 18.04 LTS Cloud Image"
create_cloudimage_template https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img 2003 "Ubuntu 20.04 LTS Cloud Image"
create_cloudimage_template https://cloud-images.ubuntu.com/impish/current/impish-server-cloudimg-amd64.img 2004 "Ubuntu 21.10 Cloud Image"

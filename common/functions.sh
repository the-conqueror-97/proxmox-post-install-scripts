#!/bin/bash

create_cloudimage_template() {
    URL=$1
    VM_ID=$2
    DESCRIPTION=$3

    IMAGE_FILE_NAME=${URL##*/} # bionic-server-cloudimg-amd64.img
    FILE_NAME=${IMAGE_FILE_NAME%.*} # bionic-server-cloudimg-amd64

    cd ~
    echo "========================================== VM Informations ============================================="
    echo "Will download: $URL"
    echo "VM ID: $VM_ID"
    echo "$DESCRIPTION"
    echo "Image name is: $IMAGE_FILE_NAME"
    echo "File name: $FILE_NAME"
    echo ${URL##*/}
    
    
    if [ ! -f "~/$IMAGE_FILE_NAME" ]; then
        echo "========================================== Downloading image ==========================================="
        wget $URL
    fi
   
    echo "========================================== Creating a new VM $VM_ID ===================================="
    qm create $VM_ID --name $FILE_NAME --memory 1024 --net0 virtio,bridge=vmbr0 --cores 1 --sockets 1 --cpu cputype=kvm64 --description "$DESCRIPTION" --kvm 1 --numa 1
    echo "========================================== Importing image into qcow format ============================="
    qm importdisk $VM_ID $IMAGE_FILE_NAME local-lvm

    echo "========================================== Setting up VM $VM_ID ========================================"
    qm set $VM_ID --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-$VM_ID-disk-0
    qm set $VM_ID --ide2 local-lvm:cloudinit
    qm set $VM_ID --boot c --bootdisk scsi0
    qm set $VM_ID --serial0 socket --vga serial0

    echo "========================================== Converting VM $VM_ID to template ============================"
    # Convert to a template
    qm template $VM_ID

    if [ -f "~/$IMAGE_FILE_NAME" ]; then
        echo "========================================== Clearing downloaded images =================================="
        rm "~/$IMAGE_FILE_NAME"
    fi
}

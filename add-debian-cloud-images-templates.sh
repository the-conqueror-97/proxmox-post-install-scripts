#!/bin/bash

source common/functions.sh

# Debian
create_cloudimage_template https://cdimage.debian.org/cdimage/openstack/current-9/debian-9-openstack-amd64.qcow2 2100 "Debian 9 Cloud Image Openstack"
create_cloudimage_template https://cdimage.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2 2101 "Debian 10 Cloud Image Openstack"
create_cloudimage_template https://cdimage.debian.org/cdimage/cloud/bullseye/latest/debian-11-genericcloud-amd64.qcow2 2102 "Debian 11 Cloud Image Generic cloud"

